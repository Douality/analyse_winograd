import requests
import re

import sys
import stanza

nlp = stanza.Pipeline(lang="fr", processors="tokenize,mwt,pos,lemma,depparse")
def validationEtRecuperation(line):
    separateurs = ['.','car','parce que']
    separateur=''
    for sep in separateurs:  #Test et trouve le seperateur
        if sep in line:
            separateur = sep
    if separateur == '':
        print("absence de comparateur : car,parceque,etc")
        return False
    phraseTemp = line.split(separateur)   #On coupe la ligne/phrase en 2 grace au separateur trouvé
    phrase = list(filter(None, phraseTemp))
    if len(phrase) != 2:
        print("fichier des phrases mal formaté")
        return False
    doc1 = nlp(phrase[0])
    doc2 = nlp(phrase[1])
    agent, verbe, patient,adv,adj,agentIc,PatientIc = '', '', '', '','','',''
    vrb = False
    neg = False

    for sentence in doc1.sentences:
        for objet in sentence.tokens:
            for elem in objet.words:
                if elem.deprel == 'nsubj': agent = elem.text
                if elem.deprel == 'obj': patient = elem.text
                if elem.upos == 'NOUN':
                    if vrb == False: agentIc = agent + elem.text 
                    if vrb == True: patientIc = patient + elem.text 
                if elem.feats == 'Polarity=Neg':
                    neg = True
                if elem.upos == 'VERB':
                    verbe = verbe + elem.lemma
                    vrb = True

    for sentence in doc2.sentences:
        for objet in sentence.tokens:
            for elem in objet.words:
                if elem.upos == 'ADV':
                    adv = elem.text
                if elem.upos == 'ADJ':
                    adj = elem.text
    if agent == '': agent = agentIc
    if patient == '': patient = patientIc
    return(agent,patient,verbe,adj,adv,neg)

def appelDirectJDM(mot, rel,mode) :
    motISO = mot.encode('iso-8859-1')
    reponse = requests.post("http://www.jeuxdemots.org/rezo.php", params={'gotermrel':motISO, 'onlyrels':rel, 'slim':1})
    reponses = re.findall("<code>(.*?)</code>", reponse.text)
    nb = 0
    resultat = []
    for rep in reponses :
        data = rep.split("|")
        if len(data) > 4 :
            # car pb avec certaines relations
            res = re.search("n1=([^|]+) .+n2=([^|]+) .+w=(\d+)", rep)
            if res :
                n1 = res.group(1)
                n2 = res.group(2)
                w = res.group(3)
                #print(n1, n2, w)
                motEnRelation = n2
                if motEnRelation == mot : motEnRelation = n1
                if not re.search("[_: ]", motEnRelation) :
                    if mode == 1:resultat.append([motEnRelation,rel,w])
                    if mode == 0:resultat.append(motEnRelation)
                    nb += 1
    return resultat

