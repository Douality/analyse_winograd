from Utilities import *
from filtre import *
from Calculs import *
from Comparaison import *
from Analyse import *
from Tests import *
###################################################
def SEM_semantique(E_explication,E_info_agent,E_info_patient,E_relation):
    v_res={}
    #partie pour les aides explication a !=0
    if E_explication>10:
        print("attention lire <-")
        print("...[adj][comparatif] X>Y")
        print("X est comparatif adj Y")
        print("ex: lent,moins -> X est moins lent que Y")

    #recuperer toutes les donnes externes utiles
    v_data=UT_all_lecture()
    if len(v_data)<=0:
        print("erreur lecture bbd -> Utilities -> All_lecture -> lecture_json")
        return

    #decompose les mots en liste pour gerer les mots qui provoquent les mots composites -> se lever -> avoir 'se' dans le coin pour s'en occuper
    v_mots={}
    v_mots["agent"]=E_info_agent["mot"].split(" ")
    v_mots["patient"]=E_info_patient["mot"].split(" ")
    v_mots["verbe"]=E_relation["verbe"].split(" ")
    if len(v_mots)<=0:
        print("erreur lecture mots -> split()")
        return

    #verifier qu'on a pas une comparaison d'equivaux et donc pas perdre de temps en calculs inutiles
    v_verif_egal=v_data["pdv"]["other"]["neutre"]
    for eg in v_verif_egal:
        if eg==E_relation["comparaison"]:
            print("EGALITE")
            v_res[0]={"r":None,"comparatif":E_relation["comparaison"],"valsup":None,"valinf":None,"rel":E_relation,"adjectif":E_relation["adjectif"],"rapport":"=","sup":E_info_agent["mot"],"inf":E_info_patient["mot"]}
            return v_res

    #si on a des composites, le mot principal -> se lever -> lever est la base de travail -> se il sert que pour le point de vue ssi verbe sinon a rien
    v_coeurs={}
    v_coeurs["agent"]=A_objet_coeur(E_info_agent["mot"],v_data["type"],v_data["coeur"])[0]
    v_coeurs["patient"]=A_objet_coeur(E_info_patient["mot"],v_data["type"],v_data["coeur"])[0]
    v_coeurs["verbe"]=A_objet_coeur(E_relation["verbe"],v_data["type"],v_data["coeur"])[0]
    if len(v_coeurs)<=0:
        print("erreur lecture source mot -> fct_coeur || st_type")
        return


    #Si non reconnu, identification categorie des mots voiture -> vehicule
    if (E_info_agent["type"] is None) or (E_info_agent["type"]=="") :
        E_info_agent["type"]=A_objet_categorie(v_coeurs["agent"],v_data["rec"])
    if (E_info_patient["type"] is None) or (E_info_patient["type"]==""):
        E_info_patient["type"]=A_objet_categorie(v_coeurs["patient"],v_data["rec"])
    E_info_agent["type"]=Filtre(E_info_agent["type"])
    E_info_patient["type"]=Filtre(E_info_patient["type"])

    #indexation des mots et elimination doublons 
    v_supers={}
    v_supers["verbe"]=A_super_mot(0,v_coeurs["verbe"],E_relation["adjectif"],v_data["hyper"])
    v_supers["verbe"]=list(set(v_supers["verbe"]))
    v_supers["agent"]=A_super_mot(1,v_coeurs["agent"],E_relation["adjectif"],v_data["hyper"])
    v_supers["agent"]=list(set(v_supers["agent"]))
    v_supers["patient"]=A_super_mot(1,v_coeurs["patient"],E_relation["adjectif"],v_data["hyper"])
    v_supers["patient"]=list(set(v_supers["patient"]))
    if len(v_supers)<=0 :
        print("bbd super imcomplete ou incompatible -> r_group")
        return

    #repere la caracteristique associe a un mot <-> ce qu'on compare dans le monde physique - par exemple la vitesse -> E_carac
    #associe la caracteristique a son index de tel sorte qu'il suffit de faire A[i] E[i] dans le parcourt des possibles plutot que de verifier si c oui(calcul) alors recuper l'index,... alors qu'on l'a avant et il suffit de la chercher mais faut cooreler avec le  mot/l'indice du la carac,... mieux vaut pas
    #----> simplifier la boucle d'analyse sur les caracterisques compar�es  qui sont faites dans la phrase
    v_E_carac=[]
    v_Association=[]
    for s in v_supers["verbe"]:
        #print("mot "+s)
        v_carac={}
        v_carac["verbe"]=A_carac_mot(s,v_data["caracteristique"])
        v_carac["agent"]=A_carac_mot(s,v_data["caracteristique"])
        v_carac["patient"]=A_carac_mot(s,v_data["caracteristique"])
        v_E_carac.append(v_carac)
        
        v_Assoc={}
        v_Assoc[s]=v_carac["verbe"]
        v_Assoc[s]=v_carac["agent"]
        v_Assoc[s]=v_carac["patient"]
        v_Association.append(v_Assoc)

    #Sortir tout ce qui ne va pas ensemble
    #Si tout indique la vitesse mais poids apparait -par exemple voiture vitesse ok poids ok c logique mais avec doubler non poids,depasser sa sort pas
    v_possibilites={}
    for i in range(0,len(v_E_carac)):
        v_possibilites[i]=(UT_intersection(v_E_carac[i]["verbe"],UT_intersection(v_E_carac[i]["agent"],v_E_carac[i]["patient"])))
    print("Possibilites de comparaison : "+str(v_possibilites))
    if len(v_possibilites)<1 :return {}
    print("----------------------")
    print("----Analyse----")
    print("\n")
    v_res={}
    v_nb_solution=0
    #Analyse sur toutes les caracterisque comparer dans la phrase
    for s in range(0,len(v_possibilites)) :
        for k in v_Association[s].keys():
            #contrainte associe au verbe
            v_contrainte=""
            v_contrainte=A_contrainte_mot(v_Association[s][k][0],v_coeurs["verbe"],v_data["contrainte"])
            if v_contrainte=="":
                print("Erreur contrainte -> r_hierachie avec : "+str(v_Association[s][k])+" & "+str(v_coeurs["verbe"]))

            #point de vue verbe -> position reel de son agent/patient
            
            v_pola_v="normal"
            if ( not (E_relation["negation"] is None)) or (not E_relation["negation"]==""):
                if E_relation["negation"]==True :
                    v_pola_v="inverse"
            else :
                v_pola_v=A_polarite_mot("verbe",k,v_data["pdv"])
            #pdv comparatif -> on se trouve a la fin le pronom qui correspond a l'agent/patient du verbe
            v_pola_adv=""
            v_pola_adv=A_polarite_mot("comparatif",E_relation["comparaison"],v_data["pdv"])
            #pdv comparatif -> on se trouve a la fin le pronom qui correspond a l'agent/patient du verbe
            #print("point de vue verbe "+E_relation["verbe"]+" : "+str(v_pola_v))
            #print("pdv comparatif "+E_relation["comparaison"]+" : "+str(v_pola_adv))
            if v_pola_v=="":
                print("Erreur fct_pola avec : verbe="+str(k))
                print("Defautl normal")
                v_pola_v="normal"
            if v_pola_adv=="":
                print("Erreur fct_pola avec : comparatif="+str(E_relation["comparaison"]))
                print("Default positif")
                v_pola_adv="positif"
            

            #on la contrainte du verbe de base mais si c pas bon, genre pronominal? il faut le prendre en compte
            if v_pola_v.count("inverse")>0:
                if v_contrainte.count("superiorite")>0:
                    v_contrainte="inferiorite"
                else:
                    v_contrainte="superiorite"
            print("contrainte verbe : "+str(v_contrainte))

            #l'identification des positions agent/verbe l'analyse, se porte sur les pronoms, non sur le verbe on prend directement en compte se fait
            #retour de l'analyse -> pronom
            if v_pola_adv.count("negatif")>0:
                if v_contrainte.count("superiorite")>0:
                    v_contrainte="inferiorite"
                else:
                    v_contrainte="superiorite"

            #qui doit etre superieur en valeur entre l'"agent et le patient dans le cas general
            v_rapport_init=""
            v_rapport_init=CP_type_comparaison(v_pola_v,v_pola_adv,v_data["comparaison"])
            print("rapport agent x patient : "+str(v_rapport_init))
            if v_rapport_init=="":
                print("Dans fct_comp erreur avec : "+str(v_pola_v)+" & "+str(v_pola_adv))
                print("Default >")
                v_rapport_init=">"

            #il va falloir verifier le rapport precedent si il est bon ou si on le change
            #conversion format cotrainte a format ralation dans la bbd
            v_cond=A_condition(v_contrainte)
            if len(v_cond)<=0:
                #si il y a aucune contrainte on peut dire tout et rien alors bon,....
                print("Dans r_hierache no condition over verbe:"+str(v_coeurs["verbe"])+" but not None equation...failure defaut equal")
                v_res[v_nb_solution]={"r":k,"comparatif":E_relation["comparaison"],"valsup":None,"valinf":None,"rel":E_relation,"adjectif":E_relation["adjectif"],"rapport":"=","sup":E_info_agent["mot"],"inf":E_info_patient["mot"]}
                v_nb_solution+=1
            else:
                #print("\n")
                #verifier la presence de valeur dans la bbd sinon dans quel cas d'erreur
                v_indice_a=Cal_indice_dico_val(E_info_agent,k,v_coeurs["agent"],v_data["valeur"])
                v_indice_pt=Cal_indice_dico_val(E_info_patient,k,v_coeurs["patient"],v_data["valeur"])

                #valeur de la caracteriqtique physique courante das agent/patient avec gestion cas d'erreur identifiees avant si erreur il y a
                v_val_a=Cal_val_mot(v_indice_a,E_info_agent,k,v_coeurs["agent"],v_data["valeur"])
                v_val_pt=Cal_val_mot(v_indice_a,E_info_patient,k,v_coeurs["patient"],v_data["valeur"])

                #rajoute la valeur des complement
                v_val_a+=Cal_val_mot_plus(E_info_agent["mot"],k,v_data["valeur"])
                v_val_pt+=Cal_val_mot_plus(E_info_patient["mot"],k,v_data["valeur"])

                #verifie encore que la rapport est coherent d'une autre maniere cas c la base de depart
                v_rapport_analyse=""
                v_rapport_analyse=CP_rapport_final(v_rapport_init,v_pola_v,v_pola_adv)
                if v_rapport_analyse=="":
                    v_rapport_analyse=v_rapport_init

                for cd in [v_cond]:#convresion en liste a 1element tjs car erreur de format avec erreur non identifie
                    #verifie si la contrainte des positon agent/pronom est verifie
                    v_cond_verif=CP_cond_verification(v_val_a,v_val_pt,cd)
                    #
                    #A ce nivau-> valeurs trouv�es
                    #          -> caracteristique commune ok
                    #          -> comparables ok
                    #          -> mots existent,....
                    #          en gros tout est bon et verifie sauf la position agent/patient qu'il faut donc echanger ou pas
                    if v_cond_verif is False :
                        print("inversion pour non condition de : "+str(cd))
                        v_pos=CP_hierachie(E_info_agent,E_info_patient,v_val_a,v_val_pt,v_contrainte)
                        v_c=v_val_a
                        v_val_a=v_val_pt
                        v_val_pt=v_c
                    else :
                        print("condition "+str(cd)+" verifie ")
                        v_pos=CP_hierachie(E_info_agent,E_info_patient,v_val_a,v_val_pt,v_contrainte)

                #stocke le resultat de l'analyse
                v_res[v_nb_solution]={"r":k,"verbe":E_relation["verbe"],"comparatif":E_relation["comparaison"],"valsup":v_val_a,"valinf":v_val_pt,"rel":E_relation,"adjectif":E_relation["adjectif"],"rapport":v_rapport_analyse,"sup":v_pos["+"],"inf":v_pos["-"]}
                #affiche(res)
                v_nb_solution+=1
                return v_res
    return {}
