# Projet de Détection de Schéma Winograd

Ce projet vise à perfectionner la détection de schémas Winograd en utilisant plusieurs fichiers Python dédiés à la comparaison, au calcul et à l'analyse. Le projet est sous la supervision de M. Lafourcade et M. Pompidor.

Nous faisons usage de la base lexicale "Jeu de Mots" pour effectuer l'analyse syntaxique.

Notre mission consiste à identifier les lacunes dans notre base de données et à développer des méthodes permettant de décoder les schémas Winograd. Cela inclut des aspects de comparaison et, potentiellement, une extension de la base de données.

Il est important de noter que ce projet relève principalement de la recherche.

## Contenu du Projet

Le projet se compose des fichiers suivants :

- `Analyse.py` : Fichier pour l'analyse des données.
- `Calculs.py` : Fichier contenant des calculs liés à la détection de schémas Winograd.
- `Comparaison.py` : Fichier pour la comparaison des résultats.
- `H.py` : Fichier contenant des fonctions liées à H.
- `Help.py` : Fichier d'aide.
- `Load.py` : Fichier pour le chargement des données.
- `Tests.py` : Fichier de tests unitaires.
- `TrySemantique5.py` : Fichier pour essayer des méthodes sémantiques.
- `Union.py` : Fichier contenant des fonctions liées à l'union.
- `Utilities.py` : Fichier contenant des utilitaires.
- `codeStanza.py` : Fichier pour le code Stanza.
- `fct_coeur.json`, `fct_comp.json`, `fct_pola.json` : Fichiers JSON contenant des données spécifiques.
- `filtre.py` : Fichier de filtrage.
- `fonctionProg.py` : Fichier contenant des fonctions liées à la programmation.
- `r_carac.json`, `r_group.json`, `r_hierarchie.json`, `r_val.json`, `resultat.json` : Fichiers JSON contenant des résultats.
- `st_reconnaissance.json`, `st_synonymes.json`, `st_time.json`, `st_type.json` : Fichiers JSON pour la reconnaissance.
- `test.txt` : Fichier de test.

## Comment Exécuter le Projet

1. Clônez ce référentiel sur votre ordinateur.
2. Assurez-vous d'avoir Python installé.
3. Exécutez les fichiers Python en utilisant votre environnement de développement ou la ligne de commande, par exemple : `python Union.py`. Ce programme fait le lien entre la partie syntaxique et sémantique.
4. Suivez les instructions spécifiques dans les fichiers pour l'analyse, les calculs, la comparaison, etc.

## Contribution

Si vous souhaitez contribuer à ce projet, n'hésitez pas à créer des pull requests, à signaler des problèmes ou à proposer des améliorations.

N'hésitez pas à contacter M. Lafourcade ou M. Pompidor de l'UM montpellier si vous souhaitez contribuer.
