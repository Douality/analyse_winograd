from TrySemantique5 import *
from codeStanza import *
import json
from Help import *

#affichage propre
def LD_affiche_propre(E_phrase,E_sem):
    v_ph=E_phrase
    v_pr='il'
    for s in GLOBAL_separateurs :
        v_phr=E_phrase.split(s)
        if len(v_phr)>1:
            v_ph=Filtre_esp(v_phr[1])
    for p in GLOBAL_pronoms :
        v_r=re.finditer(r'%s' % re.escape(p),str(v_ph))
        v_count = sum(1 for _ in v_r)
        if v_count>0 :
            v_pr=p
    print("Dans la phrase : "+str(E_phrase))
    print("Le resultat de l'analyse donne : "+str(v_pr)+" <-> "+str(E_sem["sup"]))
    print("Soit : \n->"+str(v_ph.replace(v_pr,E_sem['sup'])))
    return v_ph.replace(v_pr,E_sem['sup'])

# renommer le fonction syntaxe
def LD_lancement_syntaxe(E_nomFichier,E_nomFichierResultat):
    return mainStanza(E_nomFichier,E_nomFichierResultat)


# conversion du format des resultats de l'analyse syntaxique pour la semantique
def LD_conv(E_f):
    v_data = UT_lecture_json(True, None, E_f)
    return v_data


# renommer la fonction semantique
def LD_lancement_semantique(E_r, E_data):
    v_sem = SEM_semantique(E_r, E_data["agt"], E_data["pat"], E_data["rel"])
    return v_sem
